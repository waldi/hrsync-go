package main

import (
  "github.com/spf13/cobra"

  "gitlab.com/waldi/hrsync-go/server"
)

func init() {
  RootCmd.AddCommand(serverCmd)
}

var serverCmd = &cobra.Command{
  Use:   "server",
  Run: func(cmd *cobra.Command, args []string) {
    server := server.New()
    server.ListenAndServe()
  },
}
