package server

import (
	"net/http"
)

type Server struct {
	srv *http.Server
}

func New() *Server {
	mux := &http.ServeMux{}
	mux.Handle("/debian/", Handler("/debian/", "/srv/mirrors/debian"))
	srv := &http.Server{
		Addr:    "127.0.0.1:8080",
		Handler: mux,
	}
	return &Server{
		srv: srv,
	}
}

func (self *Server) ListenAndServe() error {
	return self.srv.ListenAndServe()
}
