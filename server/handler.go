package server

import (
	"encoding/binary"
	"fmt"
	"net/http"
	"strings"

	"github.com/golang/protobuf/proto"
	"gitlab.com/waldi/hrsync-go/protocol"
	"gitlab.com/waldi/hrsync-go/server/location"
	location_direct "gitlab.com/waldi/hrsync-go/server/location/direct"
)

type handler struct {
	url, root string
	l location.Location
}

func Handler(url, root string) http.Handler {
	return handler{
		url,
		root,
		location_direct.Location(root),
	}
}

func (self handler) ServeHTTP(response http.ResponseWriter, request *http.Request) {
	if !strings.HasPrefix(request.URL.Path, self.url) {
		return
	}
	name := strings.TrimLeft(strings.TrimPrefix(request.URL.Path, self.url), "/")
	// XXX: check for /../

	switch request.Method {
	case "GET":
		self.GET(response, request, name)
	case "PROPFIND":
		self.PROPFIND(response, request, name)
	default:
		response.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func (self handler) GET(response http.ResponseWriter, request *http.Request, name string) {
	self.l.Serve(response, request, name)
}

func (self handler) PROPFIND(response http.ResponseWriter, request *http.Request, name string) {
	response.Header().Set("Content-Type", "text/x.waldi.hrsync.metadata+protobuf")

	f := func(info protocol.MetadataResponse_Entry, err error) error {
		if err != nil {
			fmt.Println("error:", info.Names, err)
			return err
		}
		message := &protocol.MetadataResponse{
			Entries: []*protocol.MetadataResponse_Entry{&info},
		}
		m, err := proto.Marshal(message)
		binary.Write(response, binary.LittleEndian, len(m))
		response.Write(m)
		return nil
	}

	self.l.Walk(name, f)
}
