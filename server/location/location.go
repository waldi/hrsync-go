package location

import (
	"net/http"

	"gitlab.com/waldi/hrsync-go/protocol"
)

type LocationWalkFn func(info protocol.MetadataResponse_Entry, err error) error

type Location interface {
	Serve(response http.ResponseWriter, request *http.Request, name string)
	Walk(path string, walkFn LocationWalkFn) error
}
