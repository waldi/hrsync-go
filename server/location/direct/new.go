package direct

import (
	base "gitlab.com/waldi/hrsync-go/server/location"
)

type location struct {
	root string
}

func Location(l string) base.Location {
	return location{l}
}
