package direct

import (
	"fmt"
	"io"
	"net/http"
	"net/textproto"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

func (self location) Serve(w http.ResponseWriter, r *http.Request, name string) {
	f, err := os.Open(filepath.Join(self.root, name))
	if err != nil {
		msg, code := toHTTPError(err)
		http.Error(w, msg, code)
		return
	}
	defer f.Close()

	d, err := f.Stat()
	if err != nil {
		msg, code := toHTTPError(err)
		http.Error(w, msg, code)
		return
	}

	// redirect if the directory name doesn't end in a slash
	if d.IsDir() {
		url := r.URL.Path
		if url[len(url)-1] != '/' {
			localRedirect(w, r, path.Base(url)+"/")
			return
		}
	}

	if d.IsDir() {
		dirList(w, f)
		return
	}

        serveContent(w, r, d.Name(), d.ModTime(), d.Size(), f)
}

func toHTTPError(err error) (msg string, httpStatus int) {
	if os.IsNotExist(err) {
		return "404 page not found", http.StatusNotFound
	}
	if os.IsPermission(err) {
		return "403 Forbidden", http.StatusForbidden
	}
	// Default:
	return "500 Internal Server Error", http.StatusInternalServerError
}

func localRedirect(w http.ResponseWriter, r *http.Request, newPath string) {
	w.Header().Set("Location", newPath)
	w.WriteHeader(http.StatusMovedPermanently)
}

func dirList(w http.ResponseWriter, f *os.File) {
	dirs, err := f.Readdirnames(-1)
	if err != nil {
		// TODO: log err.Error() to the Server.ErrorLog, once it's possible
		// for a handler to get at its Server via the ResponseWriter. See
		// Issue 12438.
		http.Error(w, "Error reading directory", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	fmt.Fprintf(w, "<pre>\n")
	for _, name := range dirs {
		// name may contain '?' or '#', which must be escaped to remain
		// part of the URL path, and not indicate the start of a query
		// string or fragment.
		url := url.URL{Path: name}
		fmt.Fprintf(w, "<a href=\"%s\">%s</a>\n", url.String(), htmlReplacer.Replace(name))
	}
	fmt.Fprintf(w, "</pre>\n")
}

var htmlReplacer = strings.NewReplacer(
	"&", "&amp;",
	"<", "&lt;",
	">", "&gt;",
	// "&#34;" is shorter than "&quot;".
	`"`, "&#34;",
	// "&#39;" is shorter than "&apos;" and apos was not in HTML until HTML5.
	"'", "&#39;",
)

func serveContent(w http.ResponseWriter, r *http.Request, name string, modtime time.Time, size int64, content io.ReadSeeker) {
	etag := fmt.Sprintf("\"%x-%x\"", modtime.Unix(), size)
	w.Header().Set("ETag", etag)

	done := checkPreconditions(w, r, etag)
	if done {
		return
	}

	w.Header().Set("Content-Type", "application/octet-stream")
	w.Header().Set("Content-Length", strconv.FormatInt(size, 10))

	var sendContent io.Reader = content

	if r.Method != "HEAD" {
		io.CopyN(w, sendContent, size)
        }
}

// condResult is the result of an HTTP request precondition check.
// See https://tools.ietf.org/html/rfc7232 section 3.
type condResult int

const (
	condNone condResult = iota
	condTrue
	condFalse
)

func checkIfAnyMatch(match, etag string) bool {
	for {
		match = textproto.TrimString(match)
		if len(match) == 0 {
			break
		}
		if match[0] == ',' {
			match = match[1:]
			continue
		}
		if match[0] == '*' {
			return true
		}
		etag_cmp, remain := scanETag(match)
		if etag_cmp == "" {
			break
		}
		if etag_cmp == etag {
			return true
		}
		match = remain
	}

	return false
}

func checkIfMatch(r *http.Request, etag string) condResult {
	in := r.Header.Get("If-Match")
	if in == "" {
		return condNone
	}
	if checkIfAnyMatch(in, etag) {
		return condTrue
	}
	return condFalse
}

func checkIfNoneMatch(r *http.Request, etag string) condResult {
	inm := r.Header.Get("If-None-Match")
	if inm == "" {
		return condNone
	}
	if checkIfAnyMatch(inm, etag) {
		return condFalse
	}
	return condTrue
}

// checkPreconditions evaluates request preconditions and reports whether a precondition
// resulted in sending StatusNotModified or StatusPreconditionFailed.
func checkPreconditions(w http.ResponseWriter, r *http.Request, etag string) bool {
	ch := checkIfMatch(r, etag)
	if ch == condFalse {
		w.WriteHeader(http.StatusPreconditionFailed)
		return true
	}

	ch = checkIfNoneMatch(r, etag)
	if ch == condFalse {
		w.WriteHeader(http.StatusNotModified)
		return true
	}

	return false
}

// scanETag determines if a syntactically valid ETag is present at s. If so,
// the ETag and remaining text after consuming ETag is returned. Otherwise,
// it returns "", "".
func scanETag(s string) (etag string, remain string) {
	s = textproto.TrimString(s)
	start := 0
	if strings.HasPrefix(s, "W/") {
		start = 2
	}
	if len(s[start:]) < 2 || s[start] != '"' {
		return "", ""
	}
	// ETag is either W/"text" or "text".
	// See RFC 7232 2.3.
	for i := start + 1; i < len(s); i++ {
		c := s[i]
		switch {
			// Character values allowed in ETags.
		case c == 0x21 || c >= 0x23 && c <= 0x7E || c >= 0x80:
		case c == '"':
			return string(s[:i+1]), s[i+1:]
		default:
			return "", ""
		}
	}
	return "", ""
}
