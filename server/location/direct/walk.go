package direct

import (
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
	"syscall"

	protobuf_timestamp "github.com/golang/protobuf/ptypes/timestamp"
	"gitlab.com/waldi/hrsync-go/protocol"
	base "gitlab.com/waldi/hrsync-go/server/location"
)

var InvalidSymlink = errors.New("invalid symlink")

func resolveSymlink(path, relpath string) (newpath string, err error) {
	dir, _ := filepath.Split(relpath)
	newpath, err = os.Readlink(path)
	if err != nil {
		return "", err
	}
	if strings.HasPrefix(newpath, "/") || strings.HasPrefix(newpath, "../") {
		return "", InvalidSymlink
	}
	return filepath.Join(dir, newpath), nil
}

func (self location) Walk(path string, walkFn base.LocationWalkFn) error {
	type hardlink_cache_key struct {
		Dev, Ino uint64
	}

	hardlink_cache := make(map[hardlink_cache_key]string)

	f := func(path, relpath string, info os.FileInfo, err error) error {
		elem := protocol.MetadataResponse_Entry{}
		elem.Names = append(elem.Names, relpath)

		if err != nil {
			return walkFn(elem, err)
		}

		switch info_type := info.Mode() & os.ModeType; info_type {
		case 0:
			info_sys := info.Sys().(*syscall.Stat_t)
			hardlink_key := hardlink_cache_key{info_sys.Dev, info_sys.Ino}
			name_hardlink, have_hardlink := hardlink_cache[hardlink_key]
			if have_hardlink {
				elem.NamesExtra = append(elem.NamesExtra, name_hardlink)
			} else {
				hardlink_cache[hardlink_key] = relpath
			}

			elem.Type = &protocol.MetadataResponse_Entry_File{
				&protocol.MetadataResponse_Entry_FileInfo{
					Etag: fmt.Sprintf("\"%x-%x\"", info.ModTime().Unix(), info.Size()),
					Size: uint64(info.Size()),
					Mtime: &protobuf_timestamp.Timestamp{
						Seconds: info.ModTime().Unix(),
						Nanos: int32(info.ModTime().Nanosecond()),
					},
				},
			}
		case os.ModeSymlink:
			s, err := resolveSymlink(path, relpath)
			if err != nil {
				return nil
			}
			elem.Type = &protocol.MetadataResponse_Entry_Symlink{
				s,
			}
		default:
			return nil
		}

		walkFn(elem, nil)

		return nil
	}

	return walk(filepath.Join(self.root, path), path, f)
}

type WalkFunc func(path, relpath string, info os.FileInfo, err error) error

func walk(path, relpath string, walkFn WalkFunc) error {
	info, err := os.Lstat(path)
	err = walkFn(path, relpath, info, err)
	if err != nil || !info.IsDir() {
		return err
	}

	f, err := os.Open(path)
	if err != nil {
		return walkFn(path, relpath, info, err)
	}
	defer f.Close()

	for {
		names, err := f.Readdirnames(32)
		if err == io.EOF {
			break
		}
		for _, name := range names {
			new_path := filepath.Join(path, name)
			new_relpath := filepath.Join(relpath, name)
			walk(new_path, new_relpath, walkFn)
		}
	}
	return nil
}
